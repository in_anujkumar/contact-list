import styled from "styled-components";

export const List = styled.ul`
  list-style: none;
  display: flex;
  flex-direction: column;
  margin: 100px 20px 20px;
`;

export const Item = styled.li`
  display: inline-block;
  border: 1px solid #313535;
  border-radius: 10px;
  background-color: #dfe5e8;
  margin-bottom: 15px;
  transition: background-color 400ms linear;
  &:hover {
    background-color: #313535;
    a {
      color: #fff;
    }
  }
  a {
    padding: 10px;
    margin: 5px 0;
    display: block;
    text-decoration: none;
    font-weight: bold;
    color: #333;
  }
`;
