import React from "react";
import { Link } from "react-router-dom";
import { List, Item } from "./styles";

export const ContactList = ({ data }) => {
  return (
    <List>
      {data &&
        data.map(item => {
          return (
            <Item key={item.id}>
              <Link
                data-testid={`list-item${item.id}`}
                to={`/details/${item.id}`}
              >
                {item.name}
              </Link>
            </Item>
          );
        })}
      <Item key={"addMore"}>
        <Link to={`/addMore`}>{"+ Add another person"}</Link>
      </Item>
    </List>
  );
};
