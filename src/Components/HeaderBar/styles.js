import styled from "styled-components";

export const Header = styled.header`
  position: fixed;
  color: white;
  background: #313535;
  box-sizing: border-box;
  width: 100%;
  top: 0px;
  h4 {
    @media only screen and (min-width: 768px) {
      font-size: 24px;
    }
    font-size: 13px;
    font-weight: 400;
    padding: 20px;
    text-transform: uppercase;
    text-align: center;
  }
`;

export const BackButton = styled.span`
  position: absolute;
  transform: translate(-50%, -50%);
  top: 50%;
  bottom: 50%;
  left: 30px;
  cursor: pointer;
  display: block;
  font-size: 35px;
  height: 25px;
  @media only screen and (min-width: 768px) {
    display: ${props => (props.isBackVisible ? "none" : "block")};
  }
`;
