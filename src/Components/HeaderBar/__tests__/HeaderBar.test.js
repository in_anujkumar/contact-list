import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { Router } from "react-router-dom";
import { HeaderBar } from "../index";

test("test for headerbar componenet", async () => {
  const history = createMemoryHistory();
  const { getByTestId } = render(
    <Router history={history}>
      <HeaderBar />
    </Router>
  );
  const button = getByTestId("header-text");
  expect(button).toHaveTextContent("HOME");
});
