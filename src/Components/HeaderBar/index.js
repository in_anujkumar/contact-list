import React, { useContext } from "react";
import { Header, BackButton } from "./styles";
import { useLocation, useHistory } from "react-router";
import { HeaderTitle, deviceWidth } from "../../utils";
import { ContactAppContext } from "../../reducer";

export const HeaderBar = ({ title }) => {
  const location = useLocation();
  const history = useHistory();
  console.log(location);
  const { state } = useContext(ContactAppContext);
  return (
    <>
      <Header>
        {location.pathname !== "/" && !deviceWidth && (
          <BackButton
            isBackVisible={location.pathname.includes("details")}
            onClick={() => history.goBack()}
          >
            &#60;
          </BackButton>
        )}
        {location.pathname !== "/" && deviceWidth && (
          <BackButton
            isBackVisible={location.pathname.includes("details")}
            onClick={() => history.push("")}
          >
            &#60;
          </BackButton>
        )}
        <h4 data-testid="header-text">{HeaderTitle(state, location)}</h4>
      </Header>
    </>
  );
};
