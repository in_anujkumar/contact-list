import React, { useState, useEffect, useContext } from "react";
import { useParams } from "react-router";
import { ContactAppContext } from "../../reducer";
import { filteredData, generateLink, addPrefixToUrl } from "../../utils";
import { List, Item } from "./styles";

// details component
export const ContactDetails = () => {
  const { id } = useParams();
  const { state } = useContext(ContactAppContext);
  const [contactDetails, setContactDetails] = useState([]);
  useEffect(() => {
    setContactDetails(filteredData(state, id));
  }, [id, state]);

  return (
    <React.Fragment>
      {contactDetails && (
        <List>
          <Item>
            <span>ID:</span>
            <span data-testid="contact-id">{contactDetails.id}</span>
          </Item>
          <Item>
            <span>Name:</span>
            {contactDetails.name}
          </Item>
          <Item>
            <span>Username:</span>
            {contactDetails.username}
          </Item>
          <Item>
            <span>Email:</span>
            <a href={generateLink("mailto", contactDetails.email)}>
              {contactDetails.email}
            </a>
          </Item>
          <Item>
            <span>Phone:</span>
            <a href={generateLink("tel", contactDetails.phone)}>
              {contactDetails.phone}
            </a>
          </Item>
          <Item>
            <span>Website:</span>
            <a
              href={addPrefixToUrl(contactDetails.website)}
              rel="noopener noreferrer"
              target="_blank"
            >
              {contactDetails.website}
            </a>
          </Item>
        </List>
      )}
    </React.Fragment>
  );
};
