import styled from "styled-components";

export const List = styled.ul`
  list-style: none;
  display: flex;
  padding: 20px;
  flex-direction: column;
  margin-top: 100px;
`;

export const Item = styled.li`
  display: inline-block;
  padding: 10px;
  border-bottom: 1px solid #bbb3b3;
  justify-content: space-between;
  display: flex;
  span {
    margin-right: 10px;
  }
  a {
    display: inline-block;
    text-decoration: none;
    font-weight: bold;
  }
`;
