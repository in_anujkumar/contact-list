import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { ContactList } from "../../ContactList";
import { Router } from "react-router-dom";
import { Main, LocationDisplay } from "../index";

window.matchMedia =
  window.matchMedia ||
  function() {
    return {
      matches: false,
      addListener: function() {},
      removeListener: function() {}
    };
  };

let wrapper;

beforeEach(() => {
  wrapper = document.createElement("div");
  wrapper.setAttribute("id", "mainDiv");
  document.body.appendChild(wrapper);
});

afterEach(() => {
  document.body.removeChild(wrapper);
  wrapper = null;
});

const { getByTestId } = render(<Main />);

test("test for header text", async () => {
  expect(wrapper).toMatchSnapshot();
  const button = getByTestId("header-text");
  expect(button).toHaveTextContent("HOME");
});

test("add more text click event", () => {
  const history = createMemoryHistory();
  const { getByText } = render(
    <Router history={history}>
      <ContactList />
    </Router>
  );
  const node = getByText("+ Add another person");
  fireEvent.click(node);
});

test("rendering a component that uses withRouter", () => {
  const history = createMemoryHistory();
  const route = "/some-route";
  history.push(route);
  const { getByTestId } = render(
    <Router history={history}>
      <LocationDisplay />
    </Router>
  );
  expect(getByTestId("location-display")).toHaveTextContent(route);
});
