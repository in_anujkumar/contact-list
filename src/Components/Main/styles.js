import styled from "styled-components";

export const Desktop = styled.div`
  display: flex;
  transition: flex-basis 1000ms linear;
  ul {
    flex-basis: 50%;
    margin-top: 100px;
    transition: flex-basis 1000ms linear;
  }
`;
