import React, { Suspense, useEffect, useContext } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { ContactDetails } from "../ContactDetails";
import { ContactList } from "../ContactList";
import { Modal } from "../Modal";
import { myReq } from "../../request";
import { HeaderBar } from "../HeaderBar";
import { ContactAppContext } from "../../reducer";
import { Desktop } from "./styles";
import Media from "react-media";
import { withRouter } from "react-router";

const LocationDisplay = withRouter(({ location }) => (
  <div data-testid="location-display">{location.pathname}</div>
));

const Main = () => {
  const { state, dispatch } = useContext(ContactAppContext);
  // fetch initial data and save in redux store
  async function fetchData() {
    await myReq().then(data => {
      dispatch({ type: "initialData", payload: data });
    });
  }

  useEffect(
    () => {
      console.log(1);
      fetchData();
    },
    [],
    state && state.length
  );

  return (
    <Router basename="/results">
      <Suspense fallback={<div>Loading...</div>}>
        <HeaderBar />
        <Media
          queries={{
            medium: "(min-width: 320px) and (max-width: 768px)",
            large: "(min-width: 769px)"
          }}
        >
          {matches => (
            <React.Fragment>
              {matches.medium && (
                <Switch>
                  {/* home page for listing all the items */}
                  <Route
                    path="/"
                    exact
                    component={() => <ContactList data={state} />}
                  />
                  {/* page to show the selected product */}
                  <Route
                    path="/details/:id"
                    render={() => <ContactDetails />}
                  />
                  {/* <Route path="*" component={NotFound} /> */}
                  <Route
                    path="/addMore"
                    component={() => <Modal id={state && state.length} />}
                  />
                </Switch>
              )}
              {matches.large && (
                <Switch>
                  {/* home page for listing all the items */}
                  <Route
                    path="/"
                    exact
                    component={() => <ContactList data={state} />}
                  />
                  {/* page to show the selected product */}
                  <Route
                    path="/details/:id"
                    render={() => (
                      <Desktop>
                        <ContactList data={state} />
                        <ContactDetails />
                      </Desktop>
                    )}
                  />
                  <Route
                    path="/addMore"
                    component={() => <Modal id={state && state.length} />}
                  />
                </Switch>
              )}
            </React.Fragment>
          )}
        </Media>
      </Suspense>
    </Router>
  );
};

export { LocationDisplay, Main };
