import styled from "styled-components";

export const ModalWrapper = styled.div`
  position: fixed;
  z-index: 1;
  margin-top: 60px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgba(0, 0, 0, 0.4);
  transition: all 0.5s ease-in-out;
`;

export const ModalForm = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Input = styled.input`
  width: 100%;
  padding: 12px;
  box-sizing: border-box;
  border: 1px solid #ccc;
  background-color: #fff;
  margin-bottom: 15px;
  border-radius: 4px;
  font-size: 16px;
`;

export const ModalContent = styled.div`
  width: 40%;
  @media only screen and (max-width: 768px) {
    width: 80%;
  }
  box-sizing: border-box;
  margin: 10% auto;
  padding: 20px;
  background-color: #fff;
`;

export const ModalHeader = styled.h3`
  color: #333;
  margin-bottom: 15px;
`;

export const CloseButton = styled.span`
  position: absolute;
`;

export const UpdateButton = styled.button`
  border: none;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  border-radius: 4px;
  font-size: 16px;
  width: 100%;
  float: right;
  background-color: green;
  cursor: ${props => (props.isDisabled ? "default" : "pointer")};
  opacity: ${props => (props.isDisabled ? 0.4 : 1)};
  pointer-events: ${props => (props.isDisabled ? "none" : "all")};
  color: white;
  &:hover {
    opacity: 0.8;
    cursor: pointer;
  }
`;

export const Error = styled.span`
  color: #f44336;
  margin-bottom: 10px;
  display: block;
`;
