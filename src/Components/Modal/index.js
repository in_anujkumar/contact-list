import React, { useState, useContext } from "react";
import {
  ModalWrapper,
  ModalContent,
  UpdateButton,
  Input,
  ModalForm
} from "./styles";
import { useHistory } from "react-router-dom";
import { ContactAppContext } from "../../reducer";

export const Modal = props => {
  const [formData, setFormData] = useState({
    name: "",
    username: "",
    email: "",
    phone: "",
    website: ""
  });
  const { dispatch } = useContext(ContactAppContext);
  const history = useHistory();
  //   handling for mobile back
  const goHome = () => {
    history.push("");
  };
  //   inout change event
  const handleChange = event => {
    const { name, value } = event.target;
    setFormData(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  //   handle form submit
  const handleSubmit = event => {
    event.preventDefault();
    const { id } = props;
    // add unique id with form data
    const updatedFormData = { ...formData, id: id + 1 };
    // add new item on form submit
    dispatch({ type: "updateData", payload: updatedFormData });
    goHome();
  };
  return (
    <ModalWrapper id="myModal">
      <ModalContent>
        <ModalForm onSubmit={handleSubmit}>
          <Input
            type="text"
            name="name"
            required
            onChange={handleChange}
            placeholder="Enter name*"
          />
          <Input
            type="text"
            name="username"
            pattern="^[0-9a-zA-Z_.-]{3,10}"
            required
            onChange={handleChange}
            placeholder="Enter user name*"
          />
          <Input
            type="text"
            name="email"
            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
            required
            onChange={handleChange}
            placeholder="Enter email address*"
          />
          <Input
            type="number"
            name="phone"
            required
            onChange={handleChange}
            placeholder="Enter phone number*"
          />
          <Input
            type="text"
            name="website"
            required
            pattern="^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$"
            onChange={handleChange}
            placeholder="Enter website*"
          />
          <UpdateButton type="submit">Submit</UpdateButton>
        </ModalForm>
      </ModalContent>
    </ModalWrapper>
  );
};
