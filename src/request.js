import axios from "axios";

export const myReq = () => {
  const response = axios
    .get("https://jsonplaceholder.typicode.com/users")
    .then(response => {
      return response.data;
    });

  return response;
};
