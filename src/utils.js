export const filteredData = (data, id) => {
  return data && data.find(item => item.id === parseInt(id));
};

export const getName = (data, id) => {
  const customer = filteredData(data, id);
  return (customer && customer.name) || "invalid ID";
};

export const HeaderTitle = (data, location) => {
  const { pathname } = location;
  const pageID = pathname.substr(pathname.lastIndexOf("/") + 1);
  switch (pageID) {
    case "addMore":
      return "Add more person";
    case "":
      return "HOME";
    default:
      return getName(data, pageID);
  }
};

export const generateLink = (type, val) => {
  return type === "mailto" ? `mailto:${val}` : `tel:${val}`;
};

export const addPrefixToUrl = str => {
  var prefix = "//www.";
  if (str && str.substr(0, prefix.length) !== prefix) {
    str = prefix + str;
  }
  return str;
};

export const deviceWidth = () => {
  const isMobile = window.screen.width < 769 ? true : false;
  return isMobile;
};
