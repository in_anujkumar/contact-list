import React, { useReducer } from "react";

const initialState = [];
let reducer = (state = initialState, action) => {
  switch (action.type) {
    case "initialData": {
      state = action.payload;
      return state;
    }
    case "updateData": {
      return [...state, action.payload];
    }
    default:
      return state;
  }
};

// main context for app
const ContactAppContext = React.createContext(initialState);

// context provider for app
const ContactAppProvider = props => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <ContactAppContext.Provider value={{ state, dispatch }}>
      {props.children}
    </ContactAppContext.Provider>
  );
};
export { ContactAppContext, ContactAppProvider };
