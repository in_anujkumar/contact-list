import React from "react";
import { Main } from "./Components/Main";
import { ContactAppProvider } from "./reducer";
import { GlobalStyle } from "./globalStyles";

function App() {
  return (
    <div className="App">
      <ContactAppProvider>
        <Main />
      </ContactAppProvider>
      <GlobalStyle />
    </div>
  );
}

export default App;
